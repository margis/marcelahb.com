$ = jQuery

$(document).ready(function(){



	function __toggleFixedMenu(){
		//saves scroll position and windows height
		scrollPosition = $(this).scrollTop()
		windowHeight = $(window).height()

		//if the scroll position is greater than the window height show the fixed menu bar, if not then hide it
		if(scrollPosition >= windowHeight-10){
			$('.site-nav').addClass('is-fixed');
		} else {
			$('.site-nav').removeClass('is-fixed');
		}
	}

	function __toggleAnchorState(add, remove, state, type){
		$(remove).removeClass(state)
		$(add).addClass(state)
	}

	__toggleFixedMenu()

	//get scroll event
	$(window).on('scroll', function(){
		__toggleFixedMenu()
	})

	// STAR ========================================== MAIN NAVIGATION ==========================================//

	//scrolls to the id iquivalent to the anchors href using the underscore is to avoid page from blincking
	$('.site-nav a').on('click', function(){
		$(window).scrollTo($(this).attr('href')+"_", 1000)
		__toggleAnchorState(this, '.site-nav a', 'is-active')
	})

	//scrolll the page to work section
	$('.down-home').on('click', function(){
		$(window).scrollTo('#work_', 1000)
		__toggleAnchorState('.site-nav li:first-child a', '.site-nav a', 'is-active')
	})

	//scrolll the page to about section
	$('.down-work').on('click', function(){
		$(window).scrollTo('#about_', 1000)
		__toggleAnchorState('.site-nav li:nth-child(2) a', '.site-nav a', 'is-active')
	})

	// END =========================================== MAIN NAVIGATION ============================================//

	// START ===================================== WORK CATEGORY NAVIGATION =======================================//

	$($('.portfolio-nav ul li:first').attr('class').replace("portfolio-nav-item ", ".")).fadeIn();
	$('.portfolio-nav ul li:first').addClass('is-active');

	$('.portfolio-category').html($('.portfolio-nav ul li:first a').html());
	
	$('.portfolio-nav-link').on('click', function(){
		$(this).parent().addClass('is-active')
		$('.portfolio-nav-link').parent().removeClass('is-active')

		$('.portfolio-item').fadeOut(150)
		portfolioItems = $(this).attr("href").replace("#", ".")
		setTimeout("$(portfolioItems).fadeIn()", 150)
		$('.portfolio-category').html($(this).html());
		return false
	})

	// END ======================================= WORK CATEGORY NAVIGATION ==========================================//
	
	// START ========================================= WORK NAVIGATION ===============================================//

	// trickers click event to the portfolio items
	$('.portfolio-item-link').on('click', function(){
		$this = this
		$($this).parent().addClass('is-opened')

		//change url hash to "work/work-specified-in-the-data-hash-attribute"
		window.location.hash = "work/"+$($this).parent().data('hash')

		//create all DOM for the portfolio modal
		$('body').prepend('<div class="modal-bg"></div><div class="modal-nav"><a class="previous">Previous</a><a class="next">Next</a></div><div class="modal"></div>')
		__loadPortfolioItem($this)

		$('.next').on('click', function(){
			__loadPortfolioItem($('.is-opened').next().children())
			
			$('.is-opened').next().addClass('is-opened')
			$('.is-opened:first').removeClass('is-opened')

			window.location.hash = "work/"+$('.is-opened').data('hash')
			return false
		})

		$('.previous').on('click', function(){	
			__loadPortfolioItem($('.is-opened').prev().children())
			
			$('.is-opened').prev().addClass('is-opened')
			$('.is-opened:last').removeClass('is-opened')

			window.location.hash = "work/"+$('.is-opened').data('hash')
			return false
		})
		return false
	})

	function __loadPortfolioItem(anchorScope){
		$('.modal').load($(anchorScope).attr('href'), function() {
			$('.site-nav,.site-header, .site-wrapper, .slider ul#slider-img').addClass('is-blured')
			$('body').addClass('no-scroll')
			$('.modal-nav, .modal-bg, .modal, .modal *').fadeIn();

			$('.modal-close, .modal-bg').on('click', function(){
				$('.portfolio-item-link').parent().removeClass('is-opened')
				$('.site-nav, .site-header, .site-wrapper, .slider ul#slider-img').removeClass('is-blured')
				$('body').removeClass('no-scroll')
				$('.modal, .modal-bg, .modal-nav').fadeOut();
				setTimeout("$('.modal, .modal-bg, .modal-nav').remove()", 500);
				window.location.hash = "work";
				return false
			})
		});
	}

	// END =========================================== WORK NAVIGATION ===============================================//

	// START ========================================== CONTACT FORM ===============================================//

	//hides contact input label on focus
	$('.contact input, .contact textarea').on('focus', function(){
		$(this).parent().prevAll().eq(1).fadeOut('fast');
	})

	//hides contact input label on blur
	$('.contact input, .contact textarea').on('blur', function(){
		//only if input is not empty
		if($(this).val() === ''){
			$(this).parent().prevAll().eq(1).fadeIn('fast');
		}
	})

+
	// END ============================================ CONTACT FORM ===============================================//


	//chama jcycle para banner da home
	$('#slider-list') 
	.after('<div id="nav">') 
	.cycle({ 
	    fx:     'scrollLeft', 
	    speed:   1500, 
	    timeout: 5000, 
	    easing: 'easeOutCirc',
	    pager:  '#nav' 
	});

	$('#slider-img') 
	.after('<div id="nav-img">') 
	.cycle({ 
	    fx:     'fade', 
	    speed:   1500, 
	    timeout: 5000, 
	    easing: 'easeOutCirc',
	    pager:  '#nav-img' 
	});

	$( "#nav a" ).on('click', function(){
	  var index = $( "#nav a" ).index( this );
	  $("#nav-img a").eq(index).trigger('click')
	});

});  