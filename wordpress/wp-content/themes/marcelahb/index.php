<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=1100">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo MARCELAHB_CSS; ?>/normalize.css">
        <link rel="stylesheet" href="<?php echo MARCELAHB_css; ?>/main.css">
        <script src="<?php echo MARCELAHB_JS; ?>/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <header class="site-header">
            <nav class="site-nav">
                <figure class="site-logo">
                    <a href="#home">
                        <img src="<?php echo MARCELAHB_IMG; ?>/site-logo.png" alt="">
                    </a>
                </figure>
                <ul class="site-menu">
                    <li class="site-menu-item">
                        <a href="#work" class="site-menu-link">Work</a>
                    </li>
                    <li class="site-menu-item">
                        <a href="#about" class="site-menu-link">About</a>
                    </li>
                    <li class="site-menu-item">
                        <a href="#contact" class="site-menu-link">Contact</a>
                    </li>
                    <li class="site-menu-square tumblr"><a href="http://theflyingcoffee.tumblr.com/" target="_blank"></a></li>
                    <li class="site-menu-square facebbok"><a href="https://www.facebook.com/marcela.hb.1" target="_blank"></a></li>
                    <li class="site-menu-square linkedin"><a href="http://www.linkedin.com/pub/marcela-havandjian-begalli/97/aa4/388" target="_blank"></a></li>
                </ul>
                <div class="graph"></div>
            </nav>
        </header>

        <div class="site-wrapper">
            <section id="home_" class="home">
                <figure class="home-logo">
                        <img src="<?php echo MARCELAHB_IMG; ?>/home-logo.png" alt="">
                </figure>
                <div class="slider">
                    
                    <ul id="slider-list">
    
                        <?php $loop = new WP_Query( array( 'post_type' => 'banner', 'posts_per_page' => -1 ) ); ?>
                        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <li>
                            <h1>
                            <?php 
                            //remove o shortcode da galeria do conteudo 
                            $content = preg_replace('/\[(.*?)\]/', '', $post->post_content);
                            // adiciona o conteudo do post mandendo a estrutura do HTML
                            echo apply_filters('the_content', $content);
                            ?>
                            </h1>
                        </li>

                        <?php endwhile; wp_reset_query(); ?>

                    </ul>
                    <ul id="slider-img">
                        <?php $loop = new WP_Query( array( 'post_type' => 'banner', 'posts_per_page' => -1 ) );
                        while ( $loop->have_posts() ) : $loop->the_post(); 
                        
                        the_post_thumbnail( 'single-post-thumbnail' );

                        endwhile;  wp_reset_query(); ?>                    
                    </ul>

                </div>
                <a href="#works" class="down-home"></a>
            </section>
            <section id="work_" class="work">
                <header class="portfolio-header">
                    <h1 class="portfolio-category">
                        
                    </h1>
                    <nav class="portfolio-nav">
                        <ul>
                            <?php 
                            $cat_term_id = get_cat_ID( 'works' );
                            $catargs = array('orderby' => 'name', 'order' => 'ASC', 'child_of' => $cat_term_id);
                            $categories = get_categories( $catargs );
                            foreach(array_reverse($categories) as $category) { 
                                echo '<li class="portfolio-nav-item '.$category->slug.'"><a class="portfolio-nav-link" href="#'.$category->slug.'">'.$category->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    </nav>
                </header>
                <section class="portfolio">
                    <article class="portfolio-category-description">
                        <p></p>
                    </article>
                    <ul class="portfolio-list">


                        <?php 

                        $categories = get_categories( $catargs );

                        foreach (array_reverse($categories) as $category) {

                        $loop = new WP_Query( array( 'post_type' => 'work', 'posts_per_page' => -1, 'cat' => $category->cat_ID, 'order'=> 'ASC', 'orderby' => 'date') );
         
                        if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); 
         
                        $category = get_the_category(); 
                        ?>
                        <li data-hash="<?php echo($post->post_name) ?>" class="portfolio-item <?php echo $category[0]->slug; ?>">
                            <a href="<?php echo get_permalink()?>" class="portfolio-item-link">
                                <figure class="portfolio-image">
                                    <?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
                                </figure>
                                <article class="portfolio-article">
                                    <h2><?php echo the_title();?></h2>
                                    <p>
                                        <?php 
                                        echo '<p>'.get_the_excerpt().'</p>';
                                        ?>
                                    </p>
                                </article>
                            </a>
                        </li>

                        <?php endwhile; }} wp_reset_query(); ?>

                    </ul>
                </section>
                <a href="#about" class="down-work"></a>
            </section>
            <section class="about" id="about_">
                
                <h2 class="about-title">
                    <?php 
                        $page = get_posts(array( 'name' => 'about', 'post_type' => 'page'));

                        if ( $page )
                        {
                            echo apply_filters('the_content', $page[0]->post_content);
                        }
                    ?>
                </h2>
                
                <div class="column-wrapper">
                    <div class="column-left">
                        <?php 
                            $page = get_posts(array( 'name' => 'formation', 'post_type' => 'page'));

                            if ( $page )
                            {
                                echo "<h3>".$page[0]->post_title."</h3>";
                                echo apply_filters('the_content', $page[0]->post_content);
                            }
                        ?>
                        </article>
                        <div class="links hidden">
                            <?php 
                                $page = get_posts(array( 'name' => 'about', 'post_type' => 'page'));

                                if ( $page )
                                {
                                    echo '<a href="'.get_post_meta( 27, 'portfolio', true ).'" class="portfolio-link"></a>';
                                    echo '<a href="'.get_post_meta( 27, 'resume', true ).'" class="resume-link"></a>';                                  
                                }
                            ?>
                        </div>
                    </div>
                    
                    <div class="column-right">
                        <?php 
                            $page = get_posts(array( 'name' => 'previews-experiences', 'post_type' => 'page'));

                            if ( $page )
                            {
                                echo "<h3>".$page[0]->post_title."</h3>";
                                echo apply_filters('the_content', $page[0]->post_content);
                            }
                        ?>
                        </article>
                    </div>
                </div>

            </section>
            <section class="contact" id="contact_">
                <article class="contact-info">
                <?php 
                    $page = get_posts(array( 'name' => 'contact', 'post_type' => 'page'));

                    if ( $page )
                    {
                        echo apply_filters('the_content', $page[0]->post_content);
                    }
                ?>
                </article>
            </section>
            <footer class="site-footer">
                <nav class="nav-footer">
                    <ul>
                        <li class="nav-footer-item">
                            <a href="#works" class="nav-footer-item-link">Works</a>
                        </li>
                        <li class="nav-footer-item">
                            <a href="#about" class="nav-footer-item-link">About</a>
                        </li>
                        <li class="nav-footer-item">
                            <a href="#contact" class="nav-footer-item-link">Contact</a>
                        </li>
                        <div class="copyright">Copyright © 2014. All Rights Reserved</div>
                    </ul>
                </nav>
                <div class="copyright"></div>
                <figure class="footer-logo"></figure>
                <article class="footer-info">
                    <?php 
                        $page = get_posts(array( 'name' => 'footer', 'post_type' => 'page'));

                        if ( $page )
                        {
                            echo apply_filters('the_content', $page[0]->post_content);
                        }
                    ?>
                </article>
            </footer>
        </div>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="<?php echo MARCELAHB_JS; ?>/plugins.js"></script>
        <script src="<?php echo MARCELAHB_JS; ?>/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    </body>
</html>
