<?php 

    define("MARCELAHB_THEME", get_template_directory_uri(), true);
    define("MARCELAHB_CSS", get_template_directory_uri()."/css", true);
    define("MARCELAHB_JS", get_template_directory_uri()."/js", true);
    define("MARCELAHB_IMG", get_template_directory_uri()."/img", true);

    remove_shortcode('gallery');
    add_theme_support( 'post-thumbnails' ); 
    
    add_action( 'init', 'codex_book_init' );
    add_action( 'init', 'codex_banner_init' );
    /**
     * Register a book post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function codex_book_init() {
        $labels = array(
            'name'               => _x( 'Works', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'Work', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Works', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Work', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add New', 'Work', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New Work', 'your-plugin-textdomain' ),
            'new_item'           => __( 'New Work', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit Work', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View Work', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All Works', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search Works', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent Works:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No works found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No works found in Trash.', 'your-plugin-textdomain' ),
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'work' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
        );

        register_post_type( 'work', $args );
    }
    if(function_exists("register_field_group")){
        register_field_group(array (
            'id' => 'acf_wokrs',
            'title' => 'Wokrs',
            'fields' => array (
                array (
                    'key' => 'field_5379ac0317db4',
                    'label' => 'Client',
                    'name' => 'client',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'field_5379ac3217db5',
                    'label' => 'Year',
                    'name' => 'year',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => '',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'work',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'side',
                'layout' => 'no_box',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }

    function codex_banner_init() {
        $labels = array(
            'name'               => _x( 'Banners', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'banner', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Banners', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Banner', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add New', 'banner', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New banner', 'your-plugin-textdomain' ),
            'new_item'           => __( 'New banner', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit banner', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View banner', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All Banners', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search Banners', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent Banners:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No Banners found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No Banners found in Trash.', 'your-plugin-textdomain' ),
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'banner' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
        );

        register_post_type( 'banner', $args );
    }


    function myplugin_settings() {  
    // Add category metabox to page
    register_taxonomy_for_object_type('category', 'work');  
    }
     // Add to the admin_init hook of your theme functions.php file 
    add_action( 'admin_init', 'myplugin_settings' );
?>
