<div class="portfolio-single">
<?php 
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); 
			$post_content = $post->post_content;
			preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
			$array_id = explode(",", $ids[1]);
			?>
			<div class="portfolio-single-info">
				<span class="modal-close"></span>
				<h2 class="portfolio-single-title">
					<?php echo the_title(); ?>
				</h2>
				<p>
				<?php
				$teste = preg_replace('/\[(.*?)\]/', '', $post_content);
				echo apply_filters('the_content', $teste);
				?>
				</p>
			</div>
			
			<?php
			echo '<ul class="portfolio-single-image">';
			echo "<span class='client meta'> <strong>Client: </strong>".get_post_meta( $post->ID, 'client', true )."</span>";
			echo "<span class='year meta'> <strong>Year: </strong>".get_post_meta( $post->ID, 'year', true )."</span>";
			foreach($array_id as $image){
				$attachment = get_post( $image );
				?>

					<li>
						<?php echo wp_get_attachment_image( $image, 'full'); ?>
					</li>

				<?php
			}
			echo '</ul>';





		} // end while
	} // end if
?>
</div>